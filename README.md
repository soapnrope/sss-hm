## To run in production docker mode:

1. cd docker
2. docker-compose up -d

Frontend should then be available at http://localhost:3600/

## Notes:

### X-server on WSL

- WSL2 https://techcommunity.microsoft.com/t5/windows-dev-appconsult/running-wsl-gui-apps-on-windows-10/ba-p/1493242

  > Above works on WSL1 if export DISPLAY=:0 is used

- WSL1 https://virtualizationreview.com/articles/2017/02/08/graphical-programs-on-windows-subsystem-on-linux.aspx
- https://nickymeuleman.netlify.app/blog/gui-on-wsl2-cypress
