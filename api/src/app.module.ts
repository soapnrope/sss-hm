import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      `mongodb://root:rootpassword@${process.env.MONGO_URI}/sss-hm?authSource=admin`,
    ),
    UsersModule,
  ],
})
export class AppModule {}
