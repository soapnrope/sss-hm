import { MaxLength, IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @MaxLength(100)
  readonly firstName: string;

  @IsNotEmpty()
  @MaxLength(100)
  readonly lastName: string;

  @IsEmail()
  readonly eMail: string;
}
