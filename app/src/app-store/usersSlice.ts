import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { BASE_URL } from "app-config/appConfig";

type UserEntity = {
  _id: string;
  firstName: string;
  lastname: string;
  eMail: string;
};

interface UsersState {
  isLoading: boolean;
  data: UserEntity[];
}

const initialState: UsersState = {
  isLoading: false,
  data: [],
};

export const fetchUsers = createAsyncThunk("users/fetchUsers", async () => {
  const response = await fetch(`${BASE_URL}/users`);

  return (await response.json()) as UserEntity[];
});

export const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchUsers.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchUsers.rejected, (state) => {
      state.isLoading = false;
    });
    builder.addCase(fetchUsers.fulfilled, (state, action) => {
      const { payload } = action;

      state.data = payload;
      state.isLoading = false;
    });
  },
});

export default usersSlice.reducer;
