import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import users from "./usersSlice";
import userCreate from "./userCreateSlice";

export const store = configureStore({
  reducer: {
    users,
    userCreate,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
