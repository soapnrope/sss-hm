import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

import { BASE_URL } from "app-config/appConfig";

interface CreateUserData {
  firstName: string;
  lastName: string;
  eMail: string;
}

interface CreateError {
  error: string;
  message: string[];
}

interface InitialState {
  isLoading: boolean;
  isSuccessfulOperation: boolean | null;
}

const initialState: InitialState = {
  isLoading: false,
  isSuccessfulOperation: null,
};

export const createUser = createAsyncThunk<
  null,
  CreateUserData,
  {
    rejectValue: CreateError;
  }
>("userCreate/createUser", async (userData, thunkApi) => {
  const response = await fetch(`${BASE_URL}/users`, {
    method: "POST",
    body: JSON.stringify(userData),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  if (response.status === 400) {
    const createError = (await response.json()) as CreateError;
    const { error = "-" } = createError;

    toast.error(`Failed to create new user: ${error}`);
    return thunkApi.rejectWithValue(createError);
  }

  return null;
});

export const userCreateSlice = createSlice({
  name: "userCreate",
  initialState,
  reducers: {
    clearUserCreateState: () => initialState,
  },
  extraReducers: (builder) => {
    builder.addCase(createUser.pending, (state) => {
      state.isLoading = true;
      state.isSuccessfulOperation = null;
    });
    builder.addCase(createUser.rejected, (state) => {
      state.isLoading = false;
      state.isSuccessfulOperation = false;
    });
    builder.addCase(createUser.fulfilled, (state) => {
      state.isLoading = false;
      state.isSuccessfulOperation = true;
    });
  },
});

export const { clearUserCreateState } = userCreateSlice.actions;

export default userCreateSlice.reducer;
