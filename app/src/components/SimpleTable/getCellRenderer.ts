import { Column, LooseObject } from "./simpleTable.types";

function getCellRenderer(
  column: Column,
  rowData: LooseObject
): string | number | React.ReactElement {
  const { customNode } = column;
  const value = rowData[column.dataKey];

  if (customNode) {
    return customNode(rowData);
  }

  return value;
}

export default getCellRenderer;
