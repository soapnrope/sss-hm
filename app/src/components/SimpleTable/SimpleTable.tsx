import React from "react";
import clsx from "clsx";
import { Table, TableContainer, Paper } from "@material-ui/core";

import SimpleTableHead from "./components/SimpleTableHead";
import SimpleTableBody from "./components/SimpleTableBody";
import useStyles from "./useStyles";
import { SimpleTableProps } from "./simpleTable.types";

function SimpleTable(props: SimpleTableProps): React.ReactElement {
  const classes = useStyles();

  const { columns, tableData, keyField } = props;

  return (
    <>
      <Paper className={clsx(classes.root, "min-w-md sm:min-w-full")}>
        <TableContainer className={classes.tableContainer}>
          <Table>
            <SimpleTableHead columns={columns} />

            <SimpleTableBody
              keyField={keyField}
              tableData={tableData}
              columns={columns}
            />
          </Table>
        </TableContainer>
      </Paper>
    </>
  );
}

SimpleTable.defaultProps = {
  keyField: "_id",
};

export default SimpleTable;
