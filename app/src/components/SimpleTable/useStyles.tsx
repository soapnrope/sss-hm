import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    height: "100%",
    overflow: "hidden",
  },
  tableContainer: {
    maxHeight: "100%",
  },
}));

export default useStyles;
