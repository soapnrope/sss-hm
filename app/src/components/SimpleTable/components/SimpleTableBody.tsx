import React from "react";
import { TableBody, TableRow } from "@material-ui/core";

import SimpleTableRowRenderer from "./SimpleTableRowRenderer";
import { SimpleTableProps } from "../simpleTable.types";

function SimpleTableBody(props: SimpleTableProps): React.ReactElement {
  const { keyField, tableData, columns } = props;

  return (
    <TableBody>
      {tableData.map(
        (rowData): React.ReactElement => (
          <TableRow hover key={rowData[keyField]}>
            <SimpleTableRowRenderer rowData={rowData} columns={columns} />
          </TableRow>
        )
      )}
    </TableBody>
  );
}

export default SimpleTableBody;
