import React from "react";
import { TableCell, TableHead, TableRow } from "@material-ui/core";

import { Column } from "../simpleTable.types";

type Props = {
  columns: Column[];
};

function SimpleTableHead(props: Props): React.ReactElement {
  const { columns } = props;

  const columnElements = columns.map((column) => (
    <TableCell
      key={column.dataKey}
      align={column.align}
      style={{ width: column.width, background: "#5c7a8e", color: "#fff" }}
    >
      {column.label}
    </TableCell>
  ));

  return (
    <TableHead>
      <TableRow>{columnElements}</TableRow>
    </TableHead>
  );
}

export default SimpleTableHead;
