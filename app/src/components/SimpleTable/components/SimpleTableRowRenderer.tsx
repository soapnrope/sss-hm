import React from "react";
import { TableCell } from "@material-ui/core";

import getCellRenderer from "../getCellRenderer";

import { Column, LooseObject } from "../simpleTable.types";

type Props = {
  columns: Column[];
  rowData: LooseObject;
};

function SimpleTableRowRenderer(props: Props): React.ReactElement {
  const { rowData, columns } = props;

  const rowElements = columns.map((column) => {
    const { dataKey, align } = column;

    return (
      <TableCell key={dataKey} align={align}>
        {getCellRenderer(column, rowData)}
      </TableCell>
    );
  });

  return <>{rowElements}</>;
}

export default SimpleTableRowRenderer;
