interface Column {
  dataKey: string;
  align: "center" | "inherit" | "justify" | "left" | "right";
  width: string;
  label: string;
  customNode?: (arg0: LooseObject) => React.ReactElement;
}

interface LooseObject {
  [key: string]: string | number;
}

interface SimpleTableProps {
  keyField: string;
  columns: Column[];
  tableData: LooseObject[];
}

export type { Column, LooseObject, SimpleTableProps };
