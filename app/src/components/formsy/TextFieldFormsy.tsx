import React from "react";
import { TextField } from "@material-ui/core";
import { withFormsy } from "formsy-react";

function pick(o: Record<string, unknown>, fields: string[]) {
  return fields.reduce((a: Record<string, unknown>, x) => {
    if (Object.prototype.hasOwnProperty.call(o, x)) {
      a[x] = o[x];
    }

    return a;
  }, {});
}

type Props = {
  className?: string;
  autoFocus: boolean;
  label: string;
  id: string;
  type: string;
  variant: "outlined";
  fullWidth: boolean;
  value: string | number;
  errorMessage: string;
  setValue: (arg0: string | number) => void;
  placeholder: string;
};

function TextFieldFormsy(props: Props): React.ReactElement {
  const importedProps = pick(props, [
    "autoComplete",
    "autoFocus",
    "children",
    "className",
    "defaultValue",
    "disabled",
    "FormHelperTextProps",
    "fullWidth",
    "id",
    "InputLabelProps",
    "inputProps",
    "InputProps",
    "inputRef",
    "label",
    "multiline",
    "name",
    "onBlur",
    "onChange",
    "onFocus",
    "placeholder",
    "required",
    "rows",
    "rowsMax",
    "select",
    "SelectProps",
    "type",
    "variant",
    "maxLength",
  ]);

  const { value, errorMessage } = props;

  function changeValue(event: React.ChangeEvent<HTMLInputElement>) {
    props.setValue(event.currentTarget.value);
  }

  return (
    <TextField
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...importedProps}
      onChange={changeValue}
      value={value}
      error={Boolean(errorMessage)}
      helperText={errorMessage}
    />
  );
}

TextFieldFormsy.defaultProps = {
  className: undefined,
};

export default React.memo(withFormsy(TextFieldFormsy));
