import React from "react";
import PropTypes from "prop-types";
import { ValidationError } from "formsy-react/dist/interfaces";

import TextFieldFormsy from "../TextFieldFormsy";

type Props = {
  value?: string | number;
  name: string;
  label: string;
  autoFocus?: boolean;
  validations?: {
    [key: string]: unknown;
  };
  validationError?: ValidationError;
  validationErrors?: {
    [key: string]: ValidationError;
  };
  type?: string;
  required?: boolean;
  placeholder?: string;
};

const defaultProps = {
  value: "",
  type: "text",
  required: false,
  autoFocus: false,
  validations: null,
  validationError: null,
  validationErrors: {},
  placeholder: "",
};

function EditableField(props: Props): React.ReactElement {
  const {
    value = "",
    name,
    label,
    autoFocus = false,
    validations,
    validationError,
    validationErrors,
    type = "text",
    required,
    placeholder = "",
  } = props;

  return (
    <div className="my-4">
      <TextFieldFormsy
        autoFocus={autoFocus}
        label={label}
        id={name}
        name={name}
        value={value}
        type={type}
        required={required}
        variant="outlined"
        fullWidth
        validations={validations}
        validationError={validationError}
        validationErrors={validationErrors}
        placeholder={placeholder}
      />
    </div>
  );
}

EditableField.defaultProps = defaultProps;

export default EditableField;
