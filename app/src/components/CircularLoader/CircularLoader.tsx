import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) =>
  createStyles({
    progress: {
      margin: theme.spacing(2),
    },
    container: {
      width: "100%",
      height: "100%",
      position: "absolute",
      // Material-ui zIndex is usually 1300
      zIndex: 1000,
      top: 0,
      left: 0,
      backgroundColor: "#FAFAFA",
      opacity: 0.9,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    borderRadiusTop: {
      borderRadius: "8px 8px 0 0",
    },
  })
);

function CircularLoader(): React.ReactElement {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <CircularProgress
        className={classes.progress}
        size={80}
        color="secondary"
      />
    </div>
  );
}

export default CircularLoader;
