import React, { Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import routes from "app-config/routesConfig";
import CircularLoader from "components/CircularLoader";

function App(): JSX.Element {
  return (
    <Suspense fallback={<CircularLoader />}>
      <Router>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/users" />} />
          {routes.map(({ path, Component, exact }) => (
            <Route component={Component} path={path} key={path} exact={exact} />
          ))}
        </Switch>
      </Router>
    </Suspense>
  );
}

export default App;
