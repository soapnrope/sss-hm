import { lazy } from "react";

const routes = [
  {
    path: "/users",
    Component: lazy(() => import("app-pages/users/UsersList")),
    exact: true,
  },
  {
    path: "/users/create",
    Component: lazy(() => import("app-pages/users/UserCreate")),
    exact: true,
  },
];

export default routes;
