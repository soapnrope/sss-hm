import React, { useState } from "react";
import Formsy from "formsy-react";
import { useDispatch } from "react-redux";
import { Button } from "@material-ui/core";

import EditableField from "components/formsy/EditableField";
import { createUser } from "app-store/userCreateSlice";

interface FormData {
  firstName: string;
  lastName: string;
  eMail: string;
}

function UserCreateForm(): React.ReactElement {
  const dispatch = useDispatch();
  const [isFormValid, setIsFormValid] = useState(false);

  function handleSubmit(formData: FormData) {
    if (isFormValid) {
      dispatch(createUser(formData));
    }
  }

  function handleValidChange(validChange: boolean) {
    setIsFormValid(validChange);
  }

  return (
    <Formsy
      onValidSubmit={handleSubmit}
      onValid={() => handleValidChange(true)}
      onInvalid={() => handleValidChange(false)}
      name="createUserForm"
      className="flex flex-col justify-center w-full"
    >
      <EditableField
        name="firstName"
        label="First name"
        required
        validations={{
          maxLength: 100,
          isSpecialWords: true,
        }}
        validationErrors={{
          maxLength: "Max length should be 100 characters",
          isSpecialWords: "Please enter only alphabetical characters",
        }}
      />

      <EditableField
        name="lastName"
        label="Last name"
        required
        validations={{
          maxLength: 100,
          isAlpha: true,
        }}
        validationErrors={{
          maxLength: "Max length should be 100 characters",
          isAlpha: "Please enter only alphabetical characters",
        }}
      />

      <EditableField
        name="eMail"
        label="Email"
        required
        validations={{ isEmail: true }}
        validationErrors={{ isEmail: "Please enter valid email" }}
      />

      <div className="flex justify-center w-full mt-5">
        <Button
          variant="contained"
          color="primary"
          className="w-224"
          disabled={!isFormValid}
          type="submit"
        >
          Create User
        </Button>
      </div>
    </Formsy>
  );
}

export default UserCreateForm;
