import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { Link, Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";

import { RootState } from "app-store/store";
import { clearUserCreateState } from "app-store/userCreateSlice";
import CircularLoader from "components/CircularLoader";
import UserCreateForm from "./UserCreateForm";

function UserCreate(): React.ReactElement {
  const dispatch = useDispatch();
  const isCreatingUser = useSelector(
    ({ userCreate }: RootState) => userCreate.isLoading
  );
  const isSuccessfulOperation = useSelector(
    ({ userCreate }: RootState) => userCreate.isSuccessfulOperation
  );

  useEffect(() => {
    return () => {
      dispatch(clearUserCreateState());
    };
  }, [dispatch]);

  if (isSuccessfulOperation === true) {
    return <Redirect to="/users" />;
  }

  return (
    <div className="p-10 relative">
      <Link to="/users">
        <Button variant="contained" color="primary">
          Back to users list
        </Button>
      </Link>

      <Typography variant="h4" gutterBottom className="w-full text-center">
        New user creation
      </Typography>

      {isCreatingUser && <CircularLoader />}

      <UserCreateForm />
    </div>
  );
}

export default UserCreate;
