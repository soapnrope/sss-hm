import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { RootState } from "app-store/store";
import { fetchUsers } from "app-store/usersSlice";
import SimpleTable from "components/SimpleTable";
import { Column } from "components/SimpleTable/simpleTable.types";
import CircularLoader from "components/CircularLoader";

const columns: Column[] = [
  {
    label: "First name",
    dataKey: "firstName",
    align: "left",
    width: "30%",
  },
  {
    label: "Last name",
    dataKey: "lastName",
    align: "left",
    width: "30%",
  },
  {
    label: "E-mail",
    dataKey: "eMail",
    align: "left",
    width: "40%",
  },
];

function UsersListTable(): React.ReactElement {
  const dispatch = useDispatch();
  const isLoadingUsers = useSelector(({ users }: RootState) => users.isLoading);
  const usersData = useSelector(({ users }: RootState) => users.data);

  useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  return (
    <div className="relative min-h-400">
      {isLoadingUsers && <CircularLoader />}
      <SimpleTable columns={columns} tableData={usersData} />
    </div>
  );
}

export default UsersListTable;
