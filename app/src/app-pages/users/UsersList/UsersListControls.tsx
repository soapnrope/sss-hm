import React from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";

function UsersListControls(): React.ReactElement {
  return (
    <div className="mb-1">
      <Link to="/users/create">
        <Button variant="contained" color="primary">
          Create user
        </Button>
      </Link>
    </div>
  );
}

export default UsersListControls;
