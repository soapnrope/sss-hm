import React from "react";

import UsersListTable from "./UsersListTable";
import UsersListControls from "./UsersListControls";

function UsersList(): React.ReactElement {
  return (
    <div className="p-10 relative">
      <UsersListControls />
      <UsersListTable />
    </div>
  );
}

export default UsersList;
